{$include_once ../core/engine/mouse.simba}
{$include_once ../core/engine/keyboard.simba}
{$include_once ../core/constants.simba}

// Represents a tab
type
  PTab = ^TTab;
  TTab = record
  name: string;
  dtm: integer;
  box: TBox;
  hotkey: byte;
end;

function TTab.getName(): string;
begin
  exit(self.name);
end;

function TTab.getDTM(): integer;
begin
  exit(self.dtm);
end;

function TTab.getBox(): TBox;
begin
  exit(self.box);
end;

function TTab.getHotkey(): byte;
begin
  exit(self.hotkey);
end;

procedure TTab.setName(name: string);
begin
  self.name := name;
end;

procedure TTab.setDTM(dtm: string);
begin
  self.dtm := DTMFromString(dtm); // !!!
end;

procedure TTab.setBox(box: TBox);
begin
  self.box := box;
end;

procedure TTab.setHotkey(hotkey: byte);
begin
  self.hotkey := hotkey;
end;

// Creates a TTab
procedure TTab.create(name, dtm: string; box: TBox; hotkey: byte);
begin
  self.setName(name);
  self.setDTM(dtm);
  self.setBox(box);
  self.setHotkey(hotkey);
end;

procedure PTab.create(name, dtm: string; box: TBox; hotkey: byte); constref;
begin
  self^.create(name, dtm, box, hotkey);
end;

// Frees a TTab
// Only the DTM needs to be explictly freed
procedure TTab.destroy();
begin
  freeDTM(self.dtm); // !!!
end;

procedure PTab.destroy(); constref;
begin
  self^.destroy();
end;

// Returns true if the tab is active.
function TTab.isOpen(): boolean;
var
  x, y: integer;
begin
  exit(findColorTolerance(x, y, 2500208, self.getBox().x1,
                                         self.getBox().y1,
                                         self.getBox().x2,
                                         self.getBox().y2, 15));
end;

function PTab.isOpen(): boolean; constref;
begin
  exit(self^.isOpen());
end;

// Opens a tab. Returns true when the tab is made active.
// Uses hotkey -> DTM -> TBox to open the tab.
function TTab.open(): boolean;
var
  x, y, t: integer;
  m: TMouse;
  k: TKeyboard;
begin
  // Do nothing if the tab is already open
  if (self.isOpen()) then
  exit(true);

  writeLn('[Tab] Opening tab: ' + self.getName());

  k.typeByte(self.getHotkey()); // Try using hotkey first

  t:=gettimerunning()+250;
  repeat
    if (self.isOpen()) then
    exit(true);
    wait(10);
  until((gettimerunning()) > t);



  // If hotkey fails, fall back to mousing the DTM
  if findDTM(self.getDTM(), x, y, CLIENT_X1, CLIENT_Y1,
                                  CLIENT_X2, CLIENT_Y2) then
  begin
    m.move(x, y, MOUSE_LEFT);
  end else
  begin // If DTM fails, blindly mouse to the box
    m.box(self.getBox(), MOUSE_LEFT);
  end;
  t:=gettimerunning()+250;
  repeat
    if (self.isOpen()) then
    exit(true);
    wait(10);
  until((gettimerunning()) > t);
// Ensure the tabstone has time to become red
  result := self.isOpen();
end;

function PTab.open(): boolean; constref;
begin
  exit(self^.open());
end;
