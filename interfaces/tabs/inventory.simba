{$include_once ../tab.simba}

{$include_once ../../core/constants.simba}
{$include_once ../../core/engine/mouse.simba}

{$include_once ../../utilities/tbox.simba}
{$include_once ../../utilities/timer.simba}
{$include_once ../../utilities/math/grid.simba}

type
  PRSInventory = ^TRSInventory;
  TRSInventory = record
  tab: TTab;
end;

const
  INVENTORY_SLOT_LOW  = 0;
  INVENTORY_SLOT_HIGH = 27;

function TRSInventory.getTab(): PTab;
begin
  exit(@self.tab);
end;

// Creates our tab
procedure TRSInventory.create();
begin
  self.getTab().create(TAB_INVENTORY_NAME,
                       TAB_INVENTORY_DTM,
                       TAB_INVENTORY_BOX,
                       TAB_INVENTORY_HOTKEY);
end;

procedure PRSInventory.create(); constref;
begin
  self^.create();
end;

// Frees our tab
procedure TRSInventory.destroy();
begin
  self.getTab().destroy();
end;

procedure PRSInventory.destroy(); constref;
begin
  self^.destroy();
end;

// Returns bounds of the tab
function TRSInventory.getBounds(): TBox;
begin
  exit(intToBox(INVENTORY_X1, INVENTORY_Y1, INVENTORY_X2, INVENTORY_Y2));
end;

function PRSInventory.getBounds(): TBox; constref;
begin
  exit(self^.getBounds());
end;

// Returns true if tab is open
function TRSInventory.isOpen(): boolean;
begin
  exit(self.getTab().isOpen());
end;

function PRSInventory.isOpen(): boolean; constref;
begin
  exit(self^.isOpen());
end;

// Opens our tab
function TRSInventory.open(): boolean;
begin
  exit(self.getTab().open());
end;

function PRSInventory.open(): boolean; constref;
begin
  exit(self^.open());
end;

// Internal function, returns a grid of boxes representing the slots in the inventory
function TRSInventory.__grid(): TBoxArray;
begin
  result := grid(4, 7, 34, 27, 42, 36, point(585, 230));
end;

// Returns the TBox belonging to the inventory slot specified
function TRSInventory.getSlotBox(i: integer): TBox;
begin
  result := self.__grid()[i];
end;

// Function stub
function PRSInventory.getSlotBox(i: integer): TBox; constref;
begin
  exit(self^.getSlotBox(i));
end;

// Returns the TPoint in the middle of the inventory slot specified
function TRSInventory.getSlotPoint(i: integer): TPoint;
var
  b: TBox;
begin
  b := self.getSlotBox(i);
  result := b.getMiddle();
end;

// Function stub
function PRSInventory.getSlotPoint(i: integer): TPoint; constref;
begin
  exit(self^.getSlotPoint(i));
end;

// Moves the mouse to an inventory slot, optionally clicks
procedure TRSInventory.mouseSlot(i: integer; clickType: integer = MOUSE_MOVE;
                                 checkOpen: boolean = true);
var
  m: TMouse;
begin
  if (checkOpen) then
  self.open();

  m.move(self.getSlotPoint(i));
  m.click(clickType);
end;

// Function stub
procedure PRSInventory.mouseSlot(i: integer; clickType: integer = MOUSE_MOVE;
                                 checkOpen: boolean = true); constref;
begin
  self^.mouseSlot(i, clickType, checkOpen);
end;

// Returns true if an item resides in the given slot (checks for black outline)
function TRSInventory.slotOccupied(i: integer; checkOpen: boolean = true): boolean;
begin
  if (checkOpen) then
  self.open();

  result := countColor(COLOR_BLACK_OUTLINE, self.getSlotBox(i).x1,
                                            self.getSlotBox(i).y1,
                                            self.getSlotBox(i).x2,
                                            self.getSlotBox(i).y2) > 0;
end;

// Function stub
function PRSInventory.slotOccupied(i: integer; checkOpen: boolean = false): boolean; constref;
begin
  exit(self^.slotOccupied(i, checkOpen));
end;

// Returns an array containing only those slots which are occupied
function TRSInventory.getOccupiedSlots(): TIntegerArray;
var
  i, j: integer;
begin
  self.open();

  setLength(result, 28); // Range of possible values
  for i := INVENTORY_SLOT_LOW to INVENTORY_SLOT_HIGH do
  begin
    if (self.slotOccupied(i, false)) then
    begin
      inc(j);
      result[i] := i;
    end;
  end;
  setLength(result, j);
end;

// Function stub
function PRSInventory.getOccupiedSlots(): TIntegerArray; constref;
begin
  exit(self^.getOccupiedSlots());
end;

// Returns true if the inventory is full
function TRSInventory.isFull(): boolean;
begin
  result := (length(self.getOccupiedSlots()) = 28);
end;

// Function stub
function PRSInventory.isFull(): boolean; constref;
begin
  exit(self^.isFull());
end;

// Waits i*1000 milliseconds, or until inv is full, then exits
procedure TRSInventory.__wait(seconds: integer);
var
  i: integer;
begin
  for i := 1 to seconds do
  begin
    wait(1000);
    if self.isFull() then exit();
  end;
end;

// Waits and counts the amount of occupied inventory slots over a period of time
// Exits when the amount of occupied slots shifts OR alternatively when the inventory stops shifting
//
// Optionally invert the function's behavior and wait until the inventory stops shifting
// Optionally specify a rate at which to check the inventory (default 250 ms)
// Optionally specify a timeout condition for exiting (default 30 seconds)
// Optionally choose to exit the function if the inventory becomes full
//
// Example usage, to wait until the count shifts once, by any margin:
//
// p.getInventory().waitShift(); // mining a rock, you only get one ore
//
// Example usage, to wait until the count stops shifting:
//
// p.getInventory().waitShift(true); // chopping an oak tree, you get many logs
//
// Example advanced usage:
//
// p.getInventory().waitShift(true, true, 5, 60); // invert, exit when full, specify a new rate and timeout
//
procedure TRSInventory.waitShift(invert, exitWhenFull: boolean; rate, timeout: integer);
var
  lastCount: integer;
  t: Timer;
begin
  t.start();

  while (t.getTime() < (timeout * 1000)) do
  begin
    lastCount := length(self.getOccupiedSlots());
    // exiting when full is handled here, so if that is enabled, then
    // you will end up waiting 1000ms minimum
    if (exitWhenFull) then self.__wait(rate) else
    wait(rate * 1000);

    if (length(self.getOccupiedSlots()) <> lastCount) then
    begin
      if (invert) then
      continue() else
      exit();
    end else
    if (invert) then
    exit();
  end;
end;

// Function stub
procedure PRSInventory.waitShift(invert, exitWhenFull: boolean = false;
                                 rate: integer = 250; timeout: integer = 30000); constref;
begin
  self^.waitShift(invert, exitWhenFull, rate, timeout);
end;
