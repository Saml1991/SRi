{$include_once ../core/constants.simba}
{$include_once ../core/engine/ocr.simba}
{$include_once ../core/engine/mouse.simba}

{$include_once ../utilities/tbox.simba}
{$include_once ../utilities/timer.simba}
{$include_once ../utilities/tpoint.simba}
{$include_once ../utilities/tpointarray.simba}
{$include_once ../utilities/color/findcolors.simba}

// Represents the viewport
type
  TRSViewport = record
  bounds: TBox;
end;

// Represents an object found within the viewport
type
  TRSObject = record
  cts2: TCTS2Color;
  text: TStringArray;
  clickType, tries: integer;
end;

// Returns the static value of the viewport's bounding box
function TRSViewport.getBounds(): TBox;
begin
  self.bounds := intToBox(VIEWPORT_X1, VIEWPORT_Y1, VIEWPORT_X2, VIEWPORT_Y2);
  result := self.bounds;
end;

// Wrapper so we can access OCR routines from the TRSViewport type
function TRSViewport.getUpText(): string;
begin
  result := __getUpText();
end;

// Returns true if the specified string is found in current UpText
function TRSViewport.isUpText(s: string): boolean;
begin
  result := (pos(s, self.getUpText()) > 0);
end;

// Overload: same as above, takes an array of strings instead
function TRSViewport.isUpText(arr: TStringArray): boolean; overload;
var
  i:  integer;
  currText: string;
begin
  currText := self.getUpText();

  for i := 0 to (length(arr) - 1) do
  begin
    if self.isUpText(arr[i]) then exit(true);
  end;
end;

// Checks for the green color of the HP bar and returns the distance between the
// center-point of the closest HP bar and the midpoint of the viewport
//
// if the resulting point is within ~55px or so of the midpoint (with camera at
// the highest angle), we can assume it's our own HP bar
//
// Returns -1 if a HP bar was not found
function TRSViewport.getHPBarDist(): integer;
var
  atpa: T2DPointArray;
  o: TColorInfo;
  pA, pB: TPoint;
  b: TBox;
begin
  result := -1;

  o.create(COLOR_GREEN_TEXT, 1, 0.01, 0.01, self.getBounds());
  atpa := self.findObjATPA(o, 14, 4);

  if (length(atpa) > 0) then
  begin
    b :=  self.getBounds();
    pA := middleTPA(atpa[0]);
    pB := b.getMiddle();
    result := pA.dist(pB);
  end;
end;

function TRSViewport.didClick(clickType: integer): boolean;
begin
end;

// Change the viewing angle, typically call this after login
procedure TRSViewport.setAngle(high: boolean = true);
begin
  if (high) then keyDown(VK_UP) else keyDown(VK_DOWN);
  wait(3000);
  if (high) then keyUp(VK_UP) else keyUp(VK_DOWN);
end;

// Holds down ctrl and clicks the mouse wheel a few times
// Zooms out by default, can optionally zoom in
// Also typically called after login
procedure TRSViewport.setZoom(zoomOut: boolean = true);
var
  m: TMouse;
  i: integer;
begin
  m.box(self.getBounds());
  keyDown(VK_LCONTROL);
  wait(100);

  if (zoomOut) then
  begin
    writeLn('[Viewport] Zooming out');
    for i := 1 to 10 do
    m.scroll(-1);
  end else
  begin
    writeLn('[Viewport] Zooming in');
    for i := 1 to 10 do
    m.scroll(1);
  end;

  wait(100);
  keyUp(VK_LCONTROL);
end;

// Busywait until an interface matching the specified amount of COLOR_INTERFACE_TEXT
// Returns true if the interface ever appeared
function TRSViewport.waitInterface(colorAmount: integer; timeout: integer = 30000): boolean;
var
  t: Timer;
begin
  result := false;
  t.start();

  while (t.getTime() < timeout) do
  begin
    wait(250);
    if (countColor(COLOR_INTERFACE_TEXT,
                   self.getBounds().x1, self.getBounds().y1,
                   self.getBounds().x2, self.getBounds().y2)
                   = colorAmount) then exit(true);
  end;
end;

// YAFFW (Yet Another Finder Function Wrapper)
// Accepts some TColorInfo and returns a TPA of what's found
// Sorts the TPA from the center of the viewport, so any further functions
// can safely assume no sorting is required unless they transform the array
function TRSViewport.findObjTPA(c: TColorInfo): TPointArray;
var
  tpa: TPointArray;
  b: TBox;
  p: TPoint;
begin
  if findColorsSpiralTolerance(VIEWPORT_X1, VIEWPORT_Y1, tpa, c.col,
                               c.bounds, c.tol, c.cts) then
  begin
    b := self.getBounds();
    p := b.getMiddle();
    sortTPAFrom(tpa, p);
  end;

  result := tpa;
end;

// uses findObjTPA(), clusters the resulting TPA into an ATPA
// with optional width/height parameters (default array of 10x10 squares)
function TRSViewport.findObjATPA(c: TColorInfo; w, h: integer = 10): T2DPointArray;
var
  tpa: TPointArray;
begin
  tpa := self.findObjTPA(c);
  result := tpa.cluster(w, h);
end;

// combines findObjATPA() with getUpText()
// Will click on-screen if the required UpText can be found
// Sorts from midpoint of viewport, moves the mouse to the closest point
// and then works its way outward
//
// Will optionally try up to `tries' times to find the object/UpText before quitting
// Returns true if anything matching the UpText was moused over
function TRSViewport.findObj(obj: TColorInfo; text: TStringArray;
                             clickType: integer = MOUSE_LEFT; tries: integer = 1): boolean;
var
  i, j: integer;
  atpa: T2DPointArray;
  mouse: TMouse;
begin
  result := false;

  for i := 1 to tries do
  begin
    atpa := self.findObjATPA(obj);
    for j := 0 to (length(atpa) - 1) do
    begin
      mouse.move(middleTPA(atpa[j]));
      if self.isUpText(text) then
      begin
        mouse.click(clickType);
        exit(true);
      end;
    end;
  end;
end;

procedure TRSObject.create(color: TCTS2Color; text: TStringArray; clickType, tries: integer);
begin
  self.cts2      := color;
  self.text      := text;
  self.clickType := clickType;
  self.tries     := tries;

  writeLn('[Viewport] Created object: ' + text[0]);
end;

// Wrapper for the above
// Takes a TRSObject rather than TColorInfo + other parameters
function TRSViewport.findObj(obj: TRSObject): boolean; overload;
var
  colorInfo: TColorInfo;
begin
  colorInfo.create(obj.cts2.color, obj.cts2.tolerance,
                   obj.cts2.hueMod, obj.cts2.satMod, self.getBounds());

  writeLn('[Viewport] Looking for object: ' + obj.text[0]);
  result := self.findObj(colorInfo, obj.text, obj.clickType, obj.tries);
end;
