# SRi
The **S**cape **R**une **i**nclude is a library of routines that targets a [specific RuneScape private server](https://www.scaperune.net/).

Read [USAGE.md](USAGE.md) for installation, setup, and usage instructions. Check out [TODO.md](TODO.md) for a list of outstanding issues that need addressed.

## Requirements
* [Simba 1.4](https://github.com/ollydev/Simba/releases)
* [SimpleOCR](https://github.com/ollydev/SimpleOCR)
* [SRL-Fonts](https://github.com/SRL/SRL-Fonts)

The [SMART 8.6](https://github.com/BenLand100/SMART/releases/tag/v8.6) plugin was previously utilized, up until [this commit](https://gitlab.com/branon/SRi/-/commit/0e23722ee680502042b584ddc54f6af5024e8f45). 

The following people have contributed code directly to SRi:
* [Olly](https://github.com/ollydev)

SRi also uses lots of [SRL code](https://github.com/SRL), and notable projects include:
* [SRL](https://github.com/SRL/SRL) 
* [SRL-OSR](https://github.com/SRL/SRL-OSR) 
* [SRL-6](https://github.com/SRL/SRL-6)
* [AeroLib](https://github.com/J-Flight/AeroLib)
