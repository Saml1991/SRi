{ This is the script I used to farm christmas crackers during the holiday event.
  It's incredibly simplistic, mostly because SRi's capabilities at this stage
  are still quite limited. This script will:

  * Log a player in
  * Keep the player logged in by flipping between game tabs
  * Monitor the minimap for red dots (drops) that have appeared
  * Sound an alarm if any new red dots (drops) have appeared

  This script is NOT intended to fully automate the Christmas event, it merely
  indicates a christmas cracker drop, and disables SMART so as to allow a human
  player to rush over, take control, and pick the cracker up. Depending on how
  loud your speakers are, you might have to remain in the same room as your
  computer for this to have any effect.

  A couple pitfalls that I'm aware of:

  * The script sounds the alarm every time an additional red dot appears on the
    minimap. Obviously there is no way to distinguish between a christmas cracker
    and any other sort of drop, so running the script in a deserted area with
    few minimap dots visible is greatly preferred. Macroing in an area with monster
    drops, arbitrary item spawns, or anything that causes red dots to appear on
    the minimap is greatly discouraged and will not work well.

  * The script does not silence an alarm after it has set it off. You must do
    this manually, the easiest way I found was to keep a small terminal open and
    execute `pkill aplay', mute the speakers, or do both in concert.

  Other than that, everything should run pretty smoothly. The event runs until
  the 31st so there is still plenty of time to gather a few christmas crackers
  for yourself. }

program new;

{$include_once SRi/core/client.simba}

var
  c: TRSClient;
  p: TRSPlayer;
  lastCount: integer;

// Performs a basic action, intended to keep the player logged in
procedure idle();
begin
  p.getQuestList().open();
  p.getInventory().open();
end;

// Returns the number of red dots found on the minimap
function countMMcolor(): integer;
var
  mmBox: TBox := [570, 4, 728, 164];
begin
  result := countColorTolerance(1776606, mmBox.x1, mmBox.y1,
                                         mmBox.x2, mmBox.y2, 34);
end;

// Pipes random noise (static) to the default output device
// Portable and should work on any system running ALSA
procedure alert();
var
  p: TProcess;
begin
  //c.disable();

  p.init(nil);
  p.setCommandLine('/usr/bin/aplay /dev/urandom');
  p.setOptions([poUsePipes, poNoConsole]);
  p.execute();
end;

procedure shutdownHook();
begin
  p.logout();
  p.destroy();
end;

begin
  clearDebug();
  addOnTerminate('shutdownHook');

  p.create('notmyusername', 'notmypassword');

  //c.create();
  c.login(p);

  repeat
    lastCount := countMMcolor();

    wait(randomRange(3000, 6000));

    if (countMMColor() > lastCount) then
    begin
      writeLn('New drop detected! Sounding alarm...');
      alert();
    end;

    if (not p.isLoggedIn()) then
    begin
      writeLn('Logged out for some reason! Sounding alarm...');
      alert();
    end;

    idle(); // If all is well, idle and repeat
  until(false);
end.
