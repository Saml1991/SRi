# Todo

* dehardcode
* move global constants to `constants.simba`
* move non-global constants to `filename.constants.simba`?

* Casting spells
* Bank PIN
* ChooseOption
* better integrate chatbox OCR
* bidirectional chatlogger w/ IRC bot
* stealthmode

* RemoteInput
* SMART
* debug/drawing routines for both of the above

* adjust magic tab's box, others are probably fine for now
* minimap symbols and symbol finding
* flesh out rest of gametabs
* `TRSClient.didClick()`
* `TRSClient.didLoad()`

* port to vidyascape?

