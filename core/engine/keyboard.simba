{$include_once ../constants.simba}

// Represents a keyboard
type TKeyboard = record
  i: integer; // Placeholder lol, I'm sure something will go here eventually
end;          // For now it is just an excuse for me to have typed functions

(*
TKeyboard.typeByte()
~~~~~~~~

.. code-block:: pascal

    procedure TKeyboard.typeByte(key: byte);

Types one character k.

.. note::

    - by Mutant Squirrle, minor modifications by KeepBotting

Example:

.. code-block:: pascal

    keyboard.typeByte(VK_ENTER);

*)
procedure TKeyboard.typeByte(key: byte);
begin
  {$IFDEF SMART}
  if (key = 13) then
    key := 10;
  {$ENDIF}

  if (not isKeyDown(key)) then
    KeyDown(key);

  Wait(50 + Random(70));

  if isKeyDown(key) then
    KeyUp(key);
end;

// Types a string, optionally presses enter
procedure TKeyboard.send(s: string; pressEnter: boolean = true);
var
  i: integer;
begin
  for i := 1 to length(s) do
  begin // Taken from SRL-6 although the same code also exists in AeroLib
    sendKeys(s[i], 30 + random(30), 30 + random(30));
    wait(40 + random(40));
  end;

  if (pressEnter) then
  self.typeByte(VK_ENTER);
end;

// Overload: types multiple strings held in an array
// Optionally presses enter after typing each entry in the array
procedure TKeyboard.send(s: TStringArray; pressEnter: boolean = true); overload;
var
  i: integer;
begin
  for i := 0 to (length(s) - 1) do
  begin
    self.send(s[i], pressEnter);
  end;
end;
